add_action( 'wp_enqueue_scripts', 'load_jscroll' );

function load_jscroll(){
    wp_enqueue_script('jscroll', get_stylesheet_directory_uri().'/js/jscroll.js', array( 'jquery')  );
}

function infinite_scroll() {
    global $avia_config;
        echo '<script type="text/javascript">
            jQuery( ".content" ).jscroll( {
            nextSelector: "span.current + a",
            contentSelector: ".article-grid",
            autoTrigger: true
        } )
        </script>';
}
add_action( 'wp_footer', 'infinite_scroll' );